import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent } from '@delon/abc';
import { SFSchema } from '@delon/form';
import { ApiService } from '@core/services/api.service';

@Component({
  selector   : 'app-sys-log',
  templateUrl: './log.component.html',
})
export class SysLogComponent implements OnInit {
  url: any;
  searchSchema: SFSchema = {
    properties: {
      no: {
        type : 'string',
        title: '编号',
      },
    },
  };
  @ViewChild('st', { static: false }) st: STComponent;
  columns: STColumn[] = [
    { title: 'No', index: 'id' },
    { title: 'Number', type: 'tag', index: 'title' },
    // { title: 'Image', type: 'img', width: '50px', index: 'avatar' },
    // { title: 'Date', type: 'date', index: 'updatedAt' },
    {
      title  : '',
      buttons: [
        // { text: '查看', click: (item: any) => `/form/${item.id}` },
        // { text: '编辑', type: 'static', component: FormEditComponent, click: 'reload' },
      ],
    },
  ];

  constructor(
    private http: _HttpClient,
    private modal: ModalHelper,
    private apiAervices: ApiService,
  ) {
  }

  ngOnInit() {
    this.getListLog();
  }

  add() {
    // this.modal
    //   .createStatic(FormEditComponent, { i: { id: 0 } })
    //   .subscribe(() => this.st.reload());
  }

  getListLog() {
    this.apiAervices.getTopWidgets().subscribe(res => {
      this.url = res.widgets;
    });
  }

}
