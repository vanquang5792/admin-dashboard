import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { map } from 'rxjs/operators';
import { _HttpClient } from '@delon/theme';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private apiUrl: string = environment.api;

  constructor(
    private httpClient: _HttpClient,
  ) {
  }

  private url(endpoint: string): string {
    return this.apiUrl + endpoint;
  }

  getTopWidgets() {
    return this.httpClient.get<any>(this.url(`/reports/overviews/tops/widgets`)).pipe(
      map(res => {
        if (res.statusCode !== 200) {
          throw res.message;
        }
        return res.data;
      })
    );
  }

}
