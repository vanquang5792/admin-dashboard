export const environment = {
  SERVER_URL: `./`,
  production: false,
  useHash: true,
  hmr: false,
  isMockEnabled: false, // You have to switch this, when your real back-end is done
  authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',

  api: 'http://203.162.13.160:8080',
  // api: 'http://localhost:3000',
  socket: {
    host: '203.162.13.160',
    port: 15675,
    path: '/ws',
    ssl: false
  },
  preview: 'http://203.162.13.160:8083', // dev
  adwords_url: 'https://adwords.metu.vn',
  upload_serve: 'http://203.162.13.160:8084',
  // id_admin: 'NP_mTIqhx',
  id_admin: '89',
  defaultTitle: 'METU | Tùy biến nút - Hút khách hàng'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
